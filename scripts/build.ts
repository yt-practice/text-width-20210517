import esbuild from 'esbuild'

esbuild
	.build({
		entryPoints: ['src/index.ts'],
		bundle: true,
		outdir: 'dist',
		minify: true,
		platform: 'node',
		target: 'node14',
		define: { 'process.env.NODE_ENV': JSON.stringify('production') },
	})
	.catch(x => {
		console.error(x)
		process.exit(1)
	})
