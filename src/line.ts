export const line = (tag = 'div', parent: HTMLElement = document.body) => {
	const el = document.createElement(tag)
	parent.appendChild(el)
	return el
}
