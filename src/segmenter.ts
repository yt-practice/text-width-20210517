// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const Segmenter: Segmenter = (Intl as any).Segmenter

export interface Segmenter {
	// eslint-disable-next-line @typescript-eslint/no-misused-new
	new (locale: string, options: SegmenterOptions): Segmenter
	segment(string: string): Segments
}

interface Segments {
	containing(index: number): SegmentData | undefined
	[Symbol.iterator](): Iterator<SegmentData>
}

interface SegmenterOptions {
	/** 粒度 */
	granularity: 'grapheme' | 'word' | 'sentence'
}

interface SegmentData {
	/** 文字列セグメント */
	segment: string
	/** セグメントが始まる文字列のコードユニットインデックス */
	index: number
	/** セグメント化されている元の文字列 */
	input: string
	/**
	 * 粒度が "word" であり、セグメントがワード状である（文字/数字/表意文字/など）場合 true、
	 * 粒度が "word" であり、セグメントがワード状ではない（スペース/句読点/など）場合 false、
	 * 粒度が "word" でない場合 undefined。
	 */
	isWordLike?: boolean
}
