export class TextMeasurer {
	private readonly ctx: CanvasRenderingContext2D
	constructor(font = '') {
		const canvas = document.createElement('canvas')
		const ctx = canvas.getContext('2d')
		if (!ctx) throw new Error('fail')
		ctx.font = ('32px ' + font).trim()
		this.ctx = ctx
	}
	measure(text: string) {
		const metrics = this.ctx.measureText(text)
		return (
			Math.abs(metrics.actualBoundingBoxLeft - metrics.actualBoundingBoxRight) /
			32
		)
	}
}
