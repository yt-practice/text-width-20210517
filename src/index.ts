import { line } from './line'
import { Segmenter } from './segmenter'
import { TextMeasurer } from './measure-text'

export const main = async () => {
	const text = '👍🏻 は 2 文字で 👨‍👩‍👧‍👦 は 7 文字'
	document.body.innerHTML = ''
	line('style', document.head).innerHTML = 'td { text-align: end }'
	const seg = new Segmenter('ja', { granularity: 'grapheme' })
	const msrr1 = new TextMeasurer()
	const msrr2 = new TextMeasurer('monospace')
	line('p').textContent = text
	const table = line('table')
	const thead = line('thead', table)
	const tr = line('tr', thead)
	line('th', tr).textContent = 'segment'
	line('th', tr).textContent = 'measureText'
	line('th', tr).textContent = 'measureText(mono)'
	line('th', tr).textContent = '[...segment].length'
	line('th', tr).textContent = 'segment.match(/./gu).length'
	line('th', tr).textContent = 'segment.length'
	const tbody = line('tbody', table)
	for (const { segment } of seg.segment(text)) {
		if (!segment.trim()) continue
		const tr = line('tr', tbody)
		line('td', tr).textContent = segment
		line('td', tr).textContent = msrr1.measure(segment).toFixed(3)
		line('td', tr).textContent = msrr2.measure(segment).toFixed(3)
		line('td', tr).textContent = String([...segment].length)
		line('td', tr).textContent = String(segment.match(/./gu)?.length || 0)
		line('td', tr).textContent = String(segment.length)
	}
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
})
